terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.41.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_s3_bucket" "sh-bucket013" {
  bucket = "sh-bucket013"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}